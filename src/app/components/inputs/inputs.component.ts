import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TodoDataService } from 'src/app/services/todo-data.service';

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent implements OnInit {
  updateTodo: boolean = false;

  constructor(private fb: FormBuilder, private service: TodoDataService) { }

  form = this.fb.group({
    todoData: ["", [Validators.required, Validators.minLength(4)]]
  })

  ngOnInit(): void {
    this.service.updateTodo.subscribe((res: any) => {
      this.updateTodo = res;
    })

    this.service.updateInputData.subscribe((res) => {
      this.form.setValue({
        todoData: res,
      })
    })
    this.service.ngOnInit();
  }

  formSubmit() {
    const inputValue: any = this.form.value.todoData;
    this.service.setInputData(inputValue);
    this.form.reset();
  }

  updateForm() {
    const inputValue: any = this.form.value.todoData;;
    this.service.updateArray(inputValue);
    this.form.reset();
  }

}
