import { Component, OnInit } from '@angular/core';
import { TodoDataService } from 'src/app/services/todo-data.service';

@Component({
  selector: 'app-outputs',
  templateUrl: './outputs.component.html',
  styleUrls: ['./outputs.component.scss']
})
export class OutputsComponent implements OnInit {
  inputData: any = "";

  constructor(private service: TodoDataService) { }

  ngOnInit() {
    this.service.todoInput.subscribe((data: any) => {
      this.inputData = data;
    })
  }

  deleteTodo(id: number) {
    this.service.deleteTodo(id);
  }

  editTodo(id:number) {
    this.service.updateTodo.next(true);
    this.service.updateInputField(id);
  }
}
;