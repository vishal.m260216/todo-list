import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TodoDataService implements OnInit {
  todoInput = new BehaviorSubject<any>('');
  updateTodo = new BehaviorSubject<any>(false);
  updateInputData = new BehaviorSubject<any>('');
  dataArray: any[] = [];
  todoId: number = 0;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.fetchData().subscribe((res: any) => {
      if (res) {
        this.dataArray = res;
        this.todoInput.next(this.dataArray);
      }
    });
  }

  fetchData() {
    return this.http.get(
      'https://todo-list-80488-default-rtdb.firebaseio.com/todo.json'
    );
  }

  setData() {
    return this.http
      .put(
        'https://todo-list-80488-default-rtdb.firebaseio.com/todo.json',
        this.dataArray
      )
      .subscribe((res) => {});
  }

  setInputData(text: string) {
    this.dataArray.push(text);
    this.todoInput.next(this.dataArray);
    this.setData()
  }

  deleteTodo(id: number) {
    this.dataArray.splice(id, 1);
    this.todoInput.next(this.dataArray);
    this.setData()
  }

  updateInputField(id: number) {
    this.updateInputData.next(this.dataArray[id]);
    this.todoId = id;
    this.setData()
  }

  updateArray(text: string) {
    const filteredArray = this.dataArray.map((item, index) => {
      if (this.todoId === index) {
        return text;
      } else {
        return item;
      }
    });
    this.dataArray = filteredArray;
    this.todoInput.next(this.dataArray);
    this.updateTodo.next(false);
    this.setData();
  }
}
